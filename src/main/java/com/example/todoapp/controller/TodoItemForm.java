package com.example.todoapp.controller;

import java.util.List;

import com.example.todoapp.domain.model.TodoItem;

import lombok.Data;

@Data
public class TodoItemForm {
    private boolean isDone;
    private List<TodoItem> todoItems;
}