package com.example.todoapp.controller;

import java.util.Optional;

import com.example.todoapp.domain.model.TodoItem;
import com.example.todoapp.domain.repository.TodoItemRepository;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequiredArgsConstructor
@RequestMapping("/todo")
@Slf4j
public class TodoController{
    @NonNull
    private final TodoItemRepository repository;

    @GetMapping("/list")
    public String  index(@ModelAttribute final TodoItemForm todoItemForm) {
        todoItemForm.setTodoItems(this.repository.findByOrderByTitleAsc());
        log.debug("list called {}", todoItemForm);
        return "index";
    }

    @PostMapping("/finish")
    public String finish(@RequestParam("id") final long id) {
        final Optional<TodoItem> item = this.repository.findById(id);
        item.ifPresent(v -> {
            v.setDone(true);
            this.repository.save(v);
        });
        log.debug("finish called");
        return "redirect:/todo/list";
    }

    @PostMapping("/create")
    public String newItem(final TodoItem item) {
        item.setDone(false);
        this.repository.save(item);
        log.debug("create called");
        return "redirect:/todo/list";
    }
    
    @PostMapping("/delete")
    public String delete(@RequestParam("id") final long id) {
        final Optional<TodoItem> item = this.repository.findById(id);
        item.ifPresent(v -> {
            this.repository.delete(v);
        });
        log.debug("delete called");
        return "redirect:/todo/list";
    }
}
