package com.example.todoapp.domain.repository;

import java.util.List;

import com.example.todoapp.domain.model.TodoItem;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoItemRepository extends JpaRepository<TodoItem, Long> {
	public List<TodoItem> findByOrderByTitleAsc();
}